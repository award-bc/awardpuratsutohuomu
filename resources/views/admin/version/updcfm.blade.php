<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム バージョン情報修正確認</b></p>
    </div>
    
    <p><font color="red">バージョン情報を修正しますか</font></p>
    
    <form action="{{ route('admin.version.updcmp', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>id</th><td><input type="hidden" name="id" value="{{$param['id']}}">{{$param['id']}}</td></tr>
      <tr><th>バージョン名</th><td><input type="hidden" name="version_name" value="{{$param['version_name']}}">{{$param['version_name']}}</td></tr>
      <tr>
        <th>ベースバージョン</th>
        <td>
          <input type="hidden" name="base_version_id" value="{{$param['base_version_id']}}">
          {{$base_version_list[''.$param['base_version_id']]}}
        </td>
      </tr>
      <tr>
        <th>リリース種別</th>
        <td>
          <input type="hidden" name="release_type" value="{{$param['release_type']}}">
        @if (''.$param['release_type'] == '0')
          開発中
        @else
          リリース
        @endif
        </td>
      </tr>
      <tr>
        <th>カスタマイズ種別</th>
        <td>
          <input type="hidden" name="custom_type" value="{{$param['custom_type']}}">
        @if (''.$param['release_type'] == '0')
          一般
        @else
          カスタマイズ
        @endif
        </td>
      </tr>
      <tr><th>備考</th><td><input type="hidden" name="memo" value="{{$param['memo']}}">{{$param['memo']}}</td></tr>
    </table>
    <input type="submit" value="送信">
    </form>
    <p><a href="{{ route('admin.version.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
