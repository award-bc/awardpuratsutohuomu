<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Server extends Model
{
    use SoftDeletes;
    
    protected $table = 'servers';
    protected $guarded = array('id');
    public $timestamps = true;
    
}
