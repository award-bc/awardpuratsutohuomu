<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サーバー情報修正完了</b></p>
    </div>
    
    <p><font color="red">サーバー情報を修正しました</font></p>
    
    <p><a href="{{ route('admin.server.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
