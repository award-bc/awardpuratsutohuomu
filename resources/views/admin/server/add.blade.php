<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サーバー情報追加</b></p>
    </div>
    
    @for ($i = 0; $i < count($msg); $i++)
      <p><font color="red">{{$msg[$i]}}</font></p>
    @endfor
    
    <form action="{{ route('admin.server.addcfm', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>サーバー名(日本語)</th><td><input type="text" name="server_name" value="{{$param['server_name']}}"></td></tr>
      <tr><th>ホスト</th><td><input type="text" name="host" value="{{$param['host']}}"></td></tr>
      <tr><th>rootパスワード</th><td><input type="text" name="root_password" value="{{$param['root_password']}}"></td></tr>
      <tr>
        <th>サーバー種別</th>
        <td>
          <select name="service_code">
            <option value="0" @if (''.$param['service_code'] == '0') selected @endif >webサーバー</option>
            <option value="1" @if (''.$param['service_code'] == '1') selected @endif >DBサーバー</option>
          </select>
        </td>
      </tr>
      <tr><th>webサーバードキュメントルートパス</th><td><input type="text" name="web_document_root_path" value="{{$param['web_document_root_path']}}"></td></tr>
      <tr><th>webサーバーマスターデータパス</th><td><input type="text" name="web_master_path" value="{{$param['web_master_path']}}"></td></tr>
      <tr><th>webサーバーコピー元データパス</th><td><input type="text" name="web_copy_data_path" value="{{$param['web_copy_data_path']}}"></td></tr>
      <tr><th>備考</th><td><textarea name="memo" rows="10">{{$param['memo']}}</textarea></td></tr>
    </table>
    <input type="submit" value="確認">
    </form>
    <p><a href="{{ route('admin.server.list') }}">戻る</a></p>

</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
