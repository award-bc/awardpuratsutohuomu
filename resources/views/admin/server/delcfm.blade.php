<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サーバー情報削除確認</b></p>
    </div>
    
    <p><font color="red">サーバー情報を削除しますか</font></p>
    
    <form action="{{ route('admin.server.delcmp', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>id</th><td><input type="hidden" name="id" value="{{$param['id']}}">{{$param['id']}}</td></tr>
      <tr><th>サーバー名(日本語表記)</th><td><input type="hidden" name="server_name" value="{{$param['server_name']}}">{{$param['server_name']}}</td></tr>
      <tr><th>ホスト</th><td><input type="hidden" name="host" value="{{$param['host']}}">{{$param['host']}}</td></tr>
      <tr><th>rootパスワード</th><td><input type="hidden" name="root_password" value="{{$param['root_password']}}">{{$param['root_password']}}</td></tr>
      <tr>
        <th>サーバー種別</th>
        <td>
        @if (''.$param['service_code'] == '0')
          webサーバー
        @else
          DBサーバー
        @endif
        </td>
      </tr>
      <tr><th>webサーバードキュメントルートパス</th><td><input type="hidden" name="web_document_root_path" value="{{$param['web_document_root_path']}}">{{$param['web_document_root_path']}}</td></tr>
      <tr><th>webサーバーマスターデータパス</th><td><input type="hidden" name="web_master_path" value="{{$param['web_master_path']}}">{{$param['web_master_path']}}</td></tr>
      <tr><th>webサーバーコピー元データパス</th><td><input type="hidden" name="web_copy_data_path" value="{{$param['web_copy_data_path']}}">{{$param['web_copy_data_path']}}</td></tr>
      <tr><th>備考</th><td><input type="hidden" name="memo" value="{{$param['memo']}}">{{$param['memo']}}</td></tr>
    </table>
    <input type="submit" value="送信">
    </form>
    <p><a href="{{ route('admin.server.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
