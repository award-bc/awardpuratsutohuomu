<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('AWARDアカウントID');
            $table->string('name')->comment('アカウント名(アルファベット数字)');
            $table->string('email')->unique()->comment('メールアドレス');
            $table->timestamp('email_verified_at')->nullable()->comment('メールアドレス認証日時');
            $table->string('password')->comment('パスワード');
            $table->rememberToken();
            $table->string('secretariat_name_jp', 191)->comment('事務局名称(日本語)')->nullable();
            $table->string('secretariat_name_en', 191)->comment('事務局名称(英語)')->nullable();
            $table->string('secretariat_postalcode', 8)->comment('事務局郵便番号')->nullable();
            $table->longText('secretariat_staddress_jp')->comment('事務局住所(日本語)')->nullable();
            $table->longText('secretariat_staddress_en')->comment('事務局住所(英語)')->nullable();
            $table->string('secretariat_tel_jp', 20)->comment('事務局電話番号(日本表記)')->nullable();
            $table->string('secretariat_tel_en', 20)->comment('事務局電話番号(国際表記)')->nullable();
            $table->string('secretariat_mail', 191)->comment('事務局メールアドレス')->nullable();
            $table->string('secretariat_staff_name', 191)->comment('事務局担当者名')->nullable();
            $table->string('secretariat_staff_department', 191)->comment('事務局担当者所属')->nullable();
            $table->longText('memo')->comment('備考')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
