<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('サーバーID');
            $table->string('server_name', 191)->comment('サーバー名(日本語表記)');
            $table->string('host', 191)->comment('ホスト(IPアドレス or ドメイン名)');
            $table->string('root_password', 191)->comment('暗号化済みrootパスワード');
            $table->tinyInteger('service_code')->comment('サーバーサービス種別コード(0:会議サイトWEBサーバー 1:DBサーバー)')->default(0);
            $table->string('web_document_root_path', 191)->comment('webサーバードキュメントルートパス')->nullable();
            $table->string('web_master_path', 191)->comment('webサーバー会議サイトマスターデータの格納パス')->nullable();
            $table->string('web_copy_data_path', 191)->comment('webサーバー会議サイトコピー元データの格納パス')->nullable();
            $table->longText('memo')->comment('備考')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
