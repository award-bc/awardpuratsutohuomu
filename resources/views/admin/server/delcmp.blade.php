<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サーバー情報削除完了</b></p>
    </div>
    
    <p><font color="red">サーバー情報を削除しました</font></p>
    
    <p><a href="{{ route('admin.server.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
