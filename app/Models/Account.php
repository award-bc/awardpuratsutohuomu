<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Account extends Model
{
    use SoftDeletes;
    
    protected $table = 'accounts';
    protected $guarded = array('id');
    public $timestamps = true;
    
}
