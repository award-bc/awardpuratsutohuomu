<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\DBManager\VersionManager;
use App\Library\AdminCommon;
use Illuminate\Support\Facades\Schema;


class VersionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:administrator');
    }
    
    /**
     * Show the application dashboard.
     *
     */
    public function list(Request $request) {
        // DBマネージャー
        $db = new VersionManager();
        
        // バージョンリストを取得
        $base_version_list = $this->get_base_version_list();
        if ($base_version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'list' => array(),
            'base_version_list' => $base_version_list,
        );
        
        if ($db->select() == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        $page_data['list'] = $db->getRows();
        
        // ページ表示
        return view('admin.version.list', $page_data);
    }
    
    public function add(Request $request) {
        // DBマネージャー
        $db = new VersionManager();
        
        // バージョンリストを取得
        $base_version_list = $this->get_base_version_list();
        if ($base_version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
            'base_version_list' => $base_version_list
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // ページ表示
        return view('admin.version.add', $page_data);
    }
    
    public function addcfm(Request $request) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // DBマネージャー
        $db = new VersionManager();
        
        // バージョンリストを取得
        $base_version_list = $this->get_base_version_list();
        if ($base_version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
            'base_version_list' => $base_version_list
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // バリデーション
        $validation_result = $admin->validationCheck($page_data['param'], Schema::getColumnListing('versions'));
        if ($validation_result !== true) {
            if (is_array($validation_result)) {
                foreach($validation_result as $key => $value) {
                    array_push($page_data['msg'], '[' . $key . ']' . $value);
                }
            } else {
                array_push($page_data['msg'], 'エラーが発生しました');
            }
            
            // バリデートエラー(エラーメッセージ付でadd表示)
            return view('admin.version.add', $page_data);
            //return redirect()->action('Admin\VersionController@add', $page_data);
        }
        
        // ページ表示
        return view('admin.version.addcfm', $page_data);
    }
    
    public function addcmp(Request $request) {
        // DBマネージャー
        $db = new VersionManager();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // データ追加
        if ($db->insert($page_data['param'], true) !== true) {
            // エラー
            redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.version.addcmp', $page_data);
    }
    
    public function delcfm(Request $request, String $id) {
        // DBマネージャー
        $db = new VersionManager();
        
        // バージョンリストを取得
        $base_version_list = $this->get_base_version_list();
        if ($base_version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
            'base_version_list' => $base_version_list
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new VersionManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // 取得した値をセット
        $tmp_rows = $dbm->getRows();
        $page_data['param'] = $tmp_rows[0];
        
        // ページ表示
        return view('admin.version.delcfm', $page_data);
    }
    
    public function delcmp(Request $request, String $id) {
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new VersionManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // 値を削除
        if ($dbm->delete($id) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.version.delcmp', $page_data);
    }
    
    public function upd(Request $request, String $id) {
        // DBマネージャー
        $db = new VersionManager();
        
        // バージョンリストを取得
        $base_version_list = $this->get_base_version_list();
        if ($base_version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
            'base_version_list' => $base_version_list
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        if ($page_data['param']['id'] === null) {
            // パラメータの入力が無かった場合はDBより値を取得
            // 存在チェック
            $dbm = new VersionManager();
            if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
                // システムエラー
                return redirect()->action('Admin\ErrController@index');
            } else if ($dbm->getAllCount() == 0) {
                // システムエラー
                return redirect()->action('Admin\ErrController@index');
            }
            
            // 取得した値をセット
            $tmp_rows = $dbm->getRows();
            $page_data['param'] = $tmp_rows[0];
        }
        
        // ページ表示
        return view('admin.version.upd', $page_data);
    }
    
    public function updcfm(Request $request, String $id) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // DBマネージャー
        $db = new VersionManager();
        
        // バージョンリストを取得
        $base_version_list = $this->get_base_version_list();
        if ($base_version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
            'base_version_list' => $base_version_list
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new VersionManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ('' . $id != '' . $page_data['param']['id']) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バリデーション
        $validation_result = $admin->validationCheck($page_data['param'], Schema::getColumnListing('versions'));
        if ($validation_result !== true) {
            if (is_array($validation_result)) {
                foreach($validation_result as $key => $value) {
                    array_push($page_data['msg'], '[' . $key . ']' . $value);
                }
            } else {
                array_push($page_data['msg'], 'エラーが発生しました');
            }
            
            // バリデートエラー(エラーメッセージ付でupd表示)
            return view('admin.version.upd', $page_data);
            //return redirect()->action('Admin\VersionController@upd', $page_data);
        }
        
        // ページ表示
        return view('admin.version.updcfm', $page_data);
    }
    
    public function updcmp(Request $request, String $id) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('versions'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new VersionManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ('' . $id != '' . $page_data['param']['id']) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // データ更新
        $dbm->clear();
        if ($dbm->update($page_data['param'], true) !== true) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.version.updcmp', $page_data);
    }
    
    private function create_empty_param_array($table_name) {
        $table_columns = Schema::getColumnListing($table_name);
        $empty_data = array();
        
        foreach($table_columns as $value) {
            $empty_data[$value] = null;
        }
        
        return $empty_data;
    }
    
    private function get_base_version_list() {
        // DBマネージャー
        $db = new VersionManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select() == false) {
            // システムエラー
            return false;
        }
        $return_list = array('0' => '-');
        $tmp_rows = $db->getRows();
        $db->clear();
        foreach ($tmp_rows as $row) {
            $return_list[''.$row['id']] = $row['version_name'];
        }
        
        return $return_list;
    }
}
