<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム バージョン情報修正</b></p>
    </div>
    
    @for ($i = 0; $i < count($msg); $i++)
      <p><font color="red">{{$msg[$i]}}</font></p>
    @endfor
    
    <form action="{{ route('admin.version.updcfm', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>id</th><td><input type="hidden" name="id" value="{{$param['id']}}">{{$param['id']}}</td></tr>
      <tr><th>バージョン名</th><td><input type="text" name="version_name" value="{{$param['version_name']}}"></td></tr>
      <tr>
        <th>ベースバージョン</th>
        <td>
          <select name="base_version_id">
          @foreach ($base_version_list as $vid => $vname)
            <option value="{{$vid}}" @if (''.$param['base_version_id'] == ''.$vid) selected @endif >{{$vname}}</option>
          @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>リリース種別</th>
        <td>
          <select name="release_type">
            <option value="0" @if (''.$param['release_type'] == '0') selected @endif >開発中</option>
            <option value="1" @if (''.$param['release_type'] == '1') selected @endif >リリース</option>
          </select>
        </td>
      </tr>
      <tr>
        <th>カスタマイズ種別</th>
        <td>
          <select name="custom_type">
            <option value="0" @if (''.$param['custom_type'] == '0') selected @endif >一般</option>
            <option value="1" @if (''.$param['custom_type'] == '1') selected @endif >カスタマイズ</option>
          </select>
        </td>
      </tr>
      <tr><th>備考</th><td><textarea name="memo" rows="10">{{$param['memo']}}</textarea></td></tr>
    </table>
    <input type="submit" value="確認">
    </form>
    <p><a href="{{ route('admin.version.list') }}">戻る</a></p>

</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
