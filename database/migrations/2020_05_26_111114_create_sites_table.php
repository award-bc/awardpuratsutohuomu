<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('サイトID');
            $table->string('site_name', 191)->comment('サイト名(日本語表記)');
            $table->string('path_name', 191)->comment('パス名(半角英数)');
            $table->bigInteger('account_id')->comment('アカウントID');
            $table->bigInteger('web_server_id')->comment('会議WEBサイトサーバーID');
            $table->bigInteger('db_server_id')->comment('DBサーバーID');
            $table->bigInteger('version_id')->comment('サイトバージョンID');
            $table->longText('memo')->comment('備考')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
