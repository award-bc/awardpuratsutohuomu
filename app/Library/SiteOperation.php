<?php
namespace App\Library;

use PDO;
use Illuminate\Http\Request;
//use App\DBManager\QuestionnaireManager;
//use App\DBManager\QuestionManager;
//use App\DBManager\AnswerManager;
//use App\DBManager\AdminManager;
use App\Rules\AlphaNumHalf;
use Illuminate\Support\Facades\Validator;

class SiteOperation
{
    private $dbname   = '';
    private $host   = '';
    private $user     = '';
    private $password = '';
    
    public function set_db_setting($set_dbname, $set_host, $set_user, $set_password) {
        $this->dbname   = $set_dbname;
        $this->host     = $set_host;
        $this->user     = $set_user;
        $this->password = $set_password;
        return;
    }
    
    public function copy_dir($sorce_dir_path, string $target_dir_path) {
        exec('cp -a '. $sorce_dir_path . ' ' . $target_dir_path);
        return;
    }
    
    public function replacement_file($target_file_path, $replacement_value) {
        exec("mv " . $target_file_path . " " . $target_file_path . ".bk");
        
        $replacement_str = "sed ";
        $sepalator = '/';
        foreach ($replacement_value as $key => $value) {
            if(strpos($value,'/') !== false){
                //変換文字列の中に'/'が含まれている場合はセパレータを'@'に変更
                $sepalator = '@';
            } else {
                $sepalator = '/';
            }
            
            $replacement_str .= " -e 's" . $sepalator . $key . $sepalator . $value . $sepalator . "g'";
        }
        $replacement_str .= " " . $target_file_path . ".bk > " . $target_file_path;
        
        exec($replacement_str);
        exec('rm ' . $target_file_path . '.bk');
        
        return;
    }
    
    public function del_dir($del_dir_path) {
        exec('rm -rf '. $del_dir_path);
        return;
    }
    
    public function make_link($sorce_path, string $link_path) {
        exec('ln -s '. $sorce_path . ' ' . $link_path);
        return;
    }
    
    public function del_link($del_link_path) {
        exec('unlink '. $del_link_path);
        return;
    }
    
    public function create_db() {
        try{
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host, $this->user, $this->password);
            $dbh->exec('CREATE DATABASE IF NOT EXISTS ' . $this->dbname);
        } catch (PDOException $e){
            // エラー発生
            return false;
        }
        
        return true;
    }
    
    public function drop_db() {
        try{
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host, $this->user, $this->password);
            $dbh->exec('DROP DATABASE IF EXISTS ' . $this->dbname);
        } catch (PDOException $e){
            // エラー発生
            return false;
        }
        
        return true;
    }
    
    // 指定ファイルパスのsqlを実行
    public function file_exec($file_path) {
        try{
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host . '; dbname=' . $this->dbname . '; charset=utf8', $this->user, $this->password);
            $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
            // ファイルを参照して実行
            $sql = file_get_contents($file_path);
            $dbh->exec($sql);
        } catch (PDOException $e){
            // エラー発生
            return false;
        }
        
        return true;
    }
    
    
    public function get_db_site_settings($setting_name) {
        try {
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host . '; dbname=' . $this->dbname . '; charset=utf8', $this->user, $this->password);
            
            // SQL文を編集
            $sql = "SELECT * FROM t_site_settings WHERE setting_name = :setting_name";
            
            // PDOStatementクラスのインスタンスを生成
            $prepare = $dbh->prepare($sql);
            
            // 値をセット
            $prepare->bindValue(':setting_name', $setting_name, PDO::PARAM_STR);
            
            // 実行
            $prepare->execute();
            
            // 取得したデータを出力
            foreach($prepare as $value ) {
            	return $value;
            }
            
            // 該当0件
            return false;
        } catch (PDOException $e) {
            // エラー発生
            return false;
        }
        
        return false;
    }
    
    public function set_db_site_settings($setting_name, $value_en, $value_jp) {
        try {
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host . '; dbname=' . $this->dbname . '; charset=utf8', $this->user, $this->password);
            
            // SQL文を編集
            $sql = "UPDATE t_site_settings SET value_en = :value_en, value_jp = :value_jp WHERE setting_name = :setting_name";
            
            // PDOStatementクラスのインスタンスを生成
            $prepare = $dbh->prepare($sql);
            
            // 値をセット
            $prepare->bindValue(':value_en',     $value_en, PDO::PARAM_STR);
            $prepare->bindValue(':value_jp',     $value_jp, PDO::PARAM_STR);
            $prepare->bindValue(':setting_name', $setting_name, PDO::PARAM_STR);
            
            // 実行
            $prepare->execute();
            
            // コミット
            //$result = $dbh->commit();
            
            return true;
        } catch (PDOException $e) {
            // エラー発生
            return false;
        }
        
        return false;
    }
    
    public function get_db_site_limits($param_name = null) {
        try {
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host . '; dbname=' . $this->dbname . '; charset=utf8', $this->user, $this->password);
            
            // SQL文を編集
            $sql = 'SELECT * FROM site_limits';
            if ($param_name !== null) {
                $sql .= ' WHERE param_name = :param_name';
            }
            
            // PDOStatementクラスのインスタンスを生成
            $prepare = $dbh->prepare($sql);
            
            // 値をセット
            if ($param_name !== null) {
                $prepare->bindValue(':param_name', $param_name, PDO::PARAM_STR);
            }
            
            // 実行
            $prepare->execute();
            
            // 結果を取得
            // PDO::FETCH_ASSOCは、対応するカラム名にふられているものと同じキーを付けた 連想配列として取得
            $result = $prepare->fetchAll(PDO::FETCH_ASSOC);
            
            return $result;
        } catch (PDOException $e) {
            // エラー発生
            return false;
        }
        
        return false;
    }
    
    public function get_db_site_limits_update($param_name, $value) {
        try {
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host . '; dbname=' . $this->dbname . '; charset=utf8', $this->user, $this->password);
            
            // SQL文を編集
            $sql = 'UPDATE site_limits SET value = :value WHERE param_name = :param_name';
            
            // PDOStatementクラスのインスタンスを生成
            $prepare = $dbh->prepare($sql);
            
            // 値をセットして実行
            $prepare->execute(array(':value' => $value, ':param_name' => $param_name));
            
            return true;
            
        } catch (PDOException $e) {
            // エラー発生
            return false;
        }
        
        return false;
    }
    
    public function get_db_site_params($param_name = null) {
        try {
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host . '; dbname=' . $this->dbname . '; charset=utf8', $this->user, $this->password);
            
            // SQL文を編集
            $sql = 'SELECT * FROM site_params';
            if ($param_name !== null) {
                $sql .= ' WHERE param_name = :param_name';
            }
            
            // PDOStatementクラスのインスタンスを生成
            $prepare = $dbh->prepare($sql);
            
            // 値をセット
            if ($param_name !== null) {
                $prepare->bindValue(':param_name', $param_name, PDO::PARAM_STR);
            }
            
            // 実行
            $prepare->execute();
            
            // 結果を取得
            // PDO::FETCH_ASSOCは、対応するカラム名にふられているものと同じキーを付けた 連想配列として取得
            $result = $prepare->fetchAll(PDO::FETCH_ASSOC);
            
            return $result;
            
        } catch (PDOException $e) {
            // エラー発生
            return false;
        }
        
        return false;
    }
    
    public function get_db_site_params_update($param_name, $value) {
        try {
            // DB接続
            $dbh = new PDO('mysql:host='. $this->host . '; dbname=' . $this->dbname . '; charset=utf8', $this->user, $this->password);
            
            // SQL文を編集
            $sql = 'UPDATE site_params SET value = :value WHERE param_name = :param_name';
            
            // PDOStatementクラスのインスタンスを生成
            $prepare = $dbh->prepare($sql);
            
            // 値をセットして実行
            $prepare->execute(array(':value' => $value, ':param_name' => $param_name));
            
            return true;
            
        } catch (PDOException $e) {
            // エラー発生
            return false;
        }
        
        return false;
    }
}


