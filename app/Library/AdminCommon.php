<?php
namespace App\Library;
use Illuminate\Http\Request;
//use App\DBManager\QuestionnaireManager;
//use App\DBManager\QuestionManager;
//use App\DBManager\AnswerManager;
//use App\DBManager\AdminManager;
use App\Rules\AlphaNumHalf;
use Illuminate\Support\Facades\Validator;

class AdminCommon
{
    public function validationCheck(array $checkdata, array $column_list, array $add_logic = array()) {
        //////////////////////
        // バリデーションロジック定義
        //////////////////////
        $validation_logic = array(
//            'memo' => [],
            'name'                         => array('required', new AlphaNumHalf, 'max:191'),
            'email'                        => array('required', 'string', 'email', 'max:191'),
            'password'                     => array('required', 'max:191'),
            'server_name'                  => array('required', 'max:191'),
            'host'                         => array('required', 'max:191'),
            'root_password'                => array('required', 'max:191'),
            'service_code'                 => array('required', 'digits_between:0,1'),
            'web_document_root_path'       => array('max:191'),
            'web_master_path'              => array('max:191'),
            'web_copy_data_path'           => array('max:191'),
            'secretariat_name_jp'          => array('max:191'),
            'secretariat_name_en'          => array('max:191'),
            'secretariat_postalcode'       => array('max:8'),
//            'secretariat_staddress_jp' => [],
//            'secretariat_staddress_en' => [],
            'secretariat_tel_jp'           => array('max:20'),
            'secretariat_tel_en'           => array('max:20'),
            'secretariat_mail'             => array('string', 'email', 'max:191'),
            'secretariat_staff_name'       => array('max:191'),
            'secretariat_staff_department' => array('max:191'),
            'site_name'                    => array('required', 'max:191'),
            'path_name'                    => array('required', new AlphaNumHalf, 'max:191'),
            'web_server_id'                => array('required', 'integer'),
            'db_server_id'                 => array('required', 'integer'),
            'version_id'                   => array('required', 'integer'),
            'version_name'                 => array('required', 'max:191'),
            'base_version_id'              => array('integer'),
            'release_type'                 => array('required', 'digits_between:0,1'),
            'custom_type'                  => array('required', 'digits_between:0,1'),
        );
        
        // 追加ロジック指定の場合は追加する
        foreach($add_logic as $key => $value) {
            $validation_logic[$key] = array_merge($validation_logic[$key], $value);
        }
        
        // チェック対象のカラムを絞り込む
        $tmp_validation_logic = array();
        foreach ($validation_logic as $key => $value) {
            if (in_array($key, $column_list)) {
                $tmp_validation_logic[$key] = $value;
            }
        }
        $validation_logic = $tmp_validation_logic;
        
        $validator = Validator::make($checkdata, $validation_logic);
        $err_msg = array();
        
        // バリデーションエラーの場合はメッセージをセットして終了
        if ($validator->fails()) {
            foreach ($validation_logic as $key => $value) {
                if ($validator->errors()->has($key)) {
                    $err_msg[$key] = $validator->errors()->first($key);
                }
            }
            
            return $err_msg;
        }
        
        return true;
    }
}


