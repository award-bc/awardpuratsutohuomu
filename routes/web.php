<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\AdministratorController;
use App\Http\Controllers\Admin\ServerController;
use App\Http\Controllers\Admin\AccountController;
use App\Http\Controllers\Admin\VersionController;
use App\Http\Controllers\Admin\SiteController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\Admin\ErrController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'auth:administrator'], function() {

    Route::get ('menu',                     [MenuController::class, 'index'])->name('admin.menu');
    Route::post('menu',                     [MenuController::class, 'index'])->name('admin.menu');
    
    Route::get ('administrator/list',        [AdministratorController::class, 'list'])->name('admin.administrator.list');
    Route::post('administrator/add',         [AdministratorController::class, 'add'])->name('admin.administrator.add');
    Route::post('administrator/addcfm',      [AdministratorController::class, 'addcfm'])->name('admin.administrator.addcfm');
    Route::post('administrator/addcmp',      [AdministratorController::class, 'addcmp'])->name('admin.administrator.addcmp');
    Route::post('administrator/delcfm/{id}', [AdministratorController::class, 'delcfm'])->name('admin.administrator.delcfm');
    Route::post('administrator/delcmp/{id}', [AdministratorController::class, 'delcmp'])->name('admin.administrator.delcmp');
    Route::post('administrator/upd/{id}',    [AdministratorController::class, 'upd'])->name('admin.administrator.upd');
    Route::post('administrator/updcfm/{id}', [AdministratorController::class, 'updcfm'])->name('admin.administrator.updcfm');
    Route::post('administrator/updcmp/{id}', [AdministratorController::class, 'updcmp'])->name('admin.administrator.updcmp');
    
    Route::get ('server/list',        [ServerController::class, 'list'])->name('admin.server.list');
    Route::post('server/add',         [ServerController::class, 'add'])->name('admin.server.add');
    Route::post('server/addcfm',      [ServerController::class, 'addcfm'])->name('admin.server.addcfm');
    Route::post('server/addcmp',      [ServerController::class, 'addcmp'])->name('admin.server.addcmp');
    Route::post('server/delcfm/{id}', [ServerController::class, 'delcfm'])->name('admin.server.delcfm');
    Route::post('server/delcmp/{id}', [ServerController::class, 'delcmp'])->name('admin.server.delcmp');
    Route::post('server/upd/{id}',    [ServerController::class, 'upd'])->name('admin.server.upd');
    Route::post('server/updcfm/{id}', [ServerController::class, 'updcfm'])->name('admin.server.updcfm');
    Route::post('server/updcmp/{id}', [ServerController::class, 'updcmp'])->name('admin.server.updcmp');
    
    Route::get ('account/list',        [AccountController::class, 'list'])->name('admin.account.list');
    Route::post('account/add',         [AccountController::class, 'add'])->name('admin.account.add');
    Route::post('account/addcfm',      [AccountController::class, 'addcfm'])->name('admin.account.addcfm');
    Route::post('account/addcmp',      [AccountController::class, 'addcmp'])->name('admin.account.addcmp');
    Route::post('account/delcfm/{id}', [AccountController::class, 'delcfm'])->name('admin.account.delcfm');
    Route::post('account/delcmp/{id}', [AccountController::class, 'delcmp'])->name('admin.account.delcmp');
    Route::post('account/upd/{id}',    [AccountController::class, 'upd'])->name('admin.account.upd');
    Route::post('account/updcfm/{id}', [AccountController::class, 'updcfm'])->name('admin.account.updcfm');
    Route::post('account/updcmp/{id}', [AccountController::class, 'updcmp'])->name('admin.account.updcmp');
    
    Route::get ('version/list',        [VersionController::class, 'list'])->name('admin.version.list');
    Route::post('version/add',         [VersionController::class, 'add'])->name('admin.version.add');
    Route::post('version/addcfm',      [VersionController::class, 'addcfm'])->name('admin.version.addcfm');
    Route::post('version/addcmp',      [VersionController::class, 'addcmp'])->name('admin.version.addcmp');
    Route::post('version/delcfm/{id}', [VersionController::class, 'delcfm'])->name('admin.version.delcfm');
    Route::post('version/delcmp/{id}', [VersionController::class, 'delcmp'])->name('admin.version.delcmp');
    Route::post('version/upd/{id}',    [VersionController::class, 'upd'])->name('admin.version.upd');
    Route::post('version/updcfm/{id}', [VersionController::class, 'updcfm'])->name('admin.version.updcfm');
    Route::post('version/updcmp/{id}', [VersionController::class, 'updcmp'])->name('admin.version.updcmp');
    
    Route::get ('site/list',        [SiteController::class, 'list'])->name('admin.site.list');
    Route::post('site/add',         [SiteController::class, 'add'])->name('admin.site.add');
    Route::post('site/addcfm',      [SiteController::class, 'addcfm'])->name('admin.site.addcfm');
    Route::post('site/addcmp',      [SiteController::class, 'addcmp'])->name('admin.site.addcmp');
    Route::post('site/delcfm/{id}', [SiteController::class, 'delcfm'])->name('admin.site.delcfm');
    Route::post('site/delcmp/{id}', [SiteController::class, 'delcmp'])->name('admin.site.delcmp');
    Route::post('site/upd/{id}',    [SiteController::class, 'upd'])->name('admin.site.upd');
    Route::post('site/updcfm/{id}', [SiteController::class, 'updcfm'])->name('admin.site.updcfm');
    Route::post('site/updcmp/{id}', [SiteController::class, 'updcmp'])->name('admin.site.updcmp');
    
});

Route::group(['prefix' => 'admin'], function() {
    Route::get('login', [LoginController::class, 'index'])->name('admin.login');
    Route::get('err',    [ErrController::class, 'index'])->name('admin.err');
});
