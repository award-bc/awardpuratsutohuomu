<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム 管理者一覧</b></p>
    </div>
    
    <form action="{{ route('admin.administrator.add') }}" method="post">
    @csrf
    <input type="submit" value="管理者追加">
    </form>
    
    <table border="2">
      <tr><th>id</th><th>アカウント名</th><th>EMAIL</th><th>備考</th><th>修正</th><th>削除</th></tr>
    @for ($i = 0; $i < count($list); $i++)
      <tr>
      <td>{{$list[$i]['id']}}</td>
      <td>{{$list[$i]['name']}}</td>
      <td>{{$list[$i]['email']}}</td>
      <td>{{$list[$i]['memo']}}</td>
      <td><form action="{{ route('admin.administrator.upd', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="修正"></form></td>
      <td><form action="{{ route('admin.administrator.delcfm', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="削除"></form></td>
      </tr>
    @endfor
    </table>
    <p><a href="{{ route('admin.menu') }}">メニューへ戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
