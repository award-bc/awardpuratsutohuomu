<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム 管理者削除確認</b></p>
    </div>
    
    <p><font color="red">管理者を削除しますか</font></p>
    
    <form action="{{ route('admin.administrator.delcmp', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>id</th><th>アカウント名</th><th>EMAIL</th><th>パスワード</th><th>備考</th></tr>
      <tr>
      <td><input type="hidden" name="id" value="{{$param['id']}}">{{$param['id']}}</td>
      <td><input type="hidden" name="name" value="{{$param['name']}}">{{$param['name']}}</td>
      <td><input type="hidden" name="email" value="{{$param['email']}}">{{$param['email']}}</td>
      <td><input type="hidden" name="password" value="{{$param['password']}}">********</td>
      <td><input type="hidden" name="memo" value="{{$param['memo']}}">{{$param['memo']}}</td>
      </tr>
    </table>
    <input type="submit" value="送信">
    </form>
    <p><a href="{{ route('admin.administrator.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
