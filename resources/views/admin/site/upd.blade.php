<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サイト情報修正</b></p>
    </div>
    
    @for ($i = 0; $i < count($msg); $i++)
      <p><font color="red">{{$msg[$i]}}</font></p>
    @endfor
    
    <form action="{{ route('admin.site.updcfm', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>id</th><td><input type="hidden" name="id" value="{{$param['id']}}">{{$param['id']}}</td></tr>
      <tr><th>サイト名(日本語表記)</th><td><input type="text" name="site_name" value="{{$param['site_name']}}"></td></tr>
      <tr><th>URL</th><td><input type="hidden" name="path_name" value="{{$param['path_name']}}">https://{{$web_server_list[''.$param['web_server_id']]}}/{{$param['path_name']}}/</td></tr>
      <tr>
        <th>AWARDアカウント</th>
        <td>
          <input type="hidden" name="account_id" value="{{$param['account_id']}}">
          {{$account_list[''.$param['account_id']]}}
        </td>
      </tr>
      <tr>
        <th>WEBサーバー</th>
        <td>
          <input type="hidden" name="web_server_id" value="{{$param['web_server_id']}}">
          {{$web_server_list[''.$param['web_server_id']]}}
        </td>
      </tr>
      <tr>
        <th>DBサーバー</th>
        <td>
          <input type="hidden" name="db_server_id" value="{{$param['db_server_id']}}">
          {{$db_server_list[''.$param['db_server_id']]}}
        </td>
      </tr>
      <tr>
        <th>バージョン</th>
        <td>
          <input type="hidden" name="version_id" value="{{$param['version_id']}}">
          {{$version_list[''.$param['version_id']]}}
        </td>
      </tr>
      <tr><th>備考</th><td><textarea name="memo" rows="10">{{$param['memo']}}</textarea></td></tr>
    </table>
    <input type="submit" value="確認">
    </form>
    <p><a href="{{ route('admin.site.list') }}">戻る</a></p>

</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
