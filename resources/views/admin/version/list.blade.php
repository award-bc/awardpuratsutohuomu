<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム バージョン情報一覧</b></p>
    </div>
    
    <form action="{{ route('admin.version.add') }}" method="post">
    @csrf
    <input type="submit" value="バージョン情報追加">
    </form>
    
    <table border="2">
      <tr><th>id</th><th>バージョン名</th><th>ベースバージョン</th><th>リリース種別</th><th>カスタム種別</th><th>備考</th><th>修正</th><th>削除</th></tr>
    @for ($i = 0; $i < count($list); $i++)
      <tr>
      <td>{{$list[$i]['id']}}</td>
      <td>{{$list[$i]['version_name']}}</td>
      <td>{{$base_version_list[$list[$i]['base_version_id']]}}</td>
      <td>
        @if (''.$list[$i]['release_type'] == '0')
          開発中
        @else
          リリース
        @endif
      </td>
      <td>
        @if (''.$list[$i]['custom_type'] == '0')
          一般
        @else
          カスタム
        @endif
      </td>
      <td>{{$list[$i]['memo']}}</td>
      <td><form action="{{ route('admin.version.upd', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="修正"></form></td>
      <td><form action="{{ route('admin.version.delcfm', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="削除"></form></td>
      </tr>
    @endfor
    </table>
    <p><a href="{{ route('admin.menu') }}">メニューへ戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
