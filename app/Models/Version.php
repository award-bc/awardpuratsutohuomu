<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Version extends Model
{
    use SoftDeletes;
    
    protected $table = 'versions';
    protected $guarded = array('id');
    public $timestamps = true;
    
}
