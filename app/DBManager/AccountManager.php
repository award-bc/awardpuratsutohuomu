<?php

namespace App\DBManager;

use Illuminate\Database\Eloquent\Model;
use App\Models\Account;
use Illuminate\Support\Facades\Hash;

class AccountManager extends CommonDBManager
{
    // モデルを返却
    // ＊親クラスの仮想関数の実態
    protected function getModel() {
        return new \App\Models\Account;
    }
    
    // 曖昧検索をするキーのリストを返却
    // ＊親クラスの仮想関数の実態
    protected function getLikeList() {
        return array(
            'secretariat_name_jp',
            'secretariat_name_en',
            'secretariat_staddress_jp',
            'secretariat_staddress_en',
            'secretariat_staff_name',
            'secretariat_staff_department',
            'memo',
        );
    }
    protected function getEncryptList() {
        return array();
    }
    
    // 追加処理パラメータを設定してレコード追加
    public function insert_with_exparm(array $insert_row = array(), bool $commit = true) {
        // メール認証を省略
        $insert_row['email_verified_at'] = date('Y-m-d H:i:s');
        
        // パスワードをハッシュ化して格納
        $insert_row['password'] = Hash::make($insert_row['password']);
        
        return $this->insert($insert_row, $commit);
    }
    // 追加処理パラメータを設定してレコード更新
    public function update_with_exparm(array $update_row = array(), bool $commit = true) {
        // パスワードをハッシュ化して格納
        $update_row['password'] = Hash::make($update_row['password']);
        
        return $this->update($update_row, $commit);
    }
}
