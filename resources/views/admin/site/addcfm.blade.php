<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サイト情報追加確認</b></p>
    </div>
    
    <p><font color="red">サイト情報を追加しますか</font></p>
    
    <form action="{{ route('admin.site.addcmp', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>サイト名(日本語表記)</th><td><input type="hidden" name="site_name" value="{{$param['site_name']}}">{{$param['site_name']}}</td></tr>
      <tr><th>URL</th><td><input type="hidden" name="path_name" value="{{$param['path_name']}}">https://(ドメイン名)/{{$param['path_name']}}/</td></tr>
      <tr>
        <th>AWARDアカウント</th>
        <td>
          <input type="hidden" name="account_id" value="{{$param['account_id']}}">
          {{$account_list[''.$param['account_id']]}}
        </td>
      </tr>
      <tr>
        <th>WEBサーバー</th>
        <td>
          <input type="hidden" name="web_server_id" value="{{$param['web_server_id']}}">
          {{$web_server_list[''.$param['web_server_id']]}}
        </td>
      </tr>
      <tr>
        <th>DBサーバー</th>
        <td>
          <input type="hidden" name="db_server_id" value="{{$param['db_server_id']}}">
          {{$db_server_list[''.$param['db_server_id']]}}
        </td>
      </tr>
      <tr>
        <th>バージョン</th>
        <td>
          <input type="hidden" name="version_id" value="{{$param['version_id']}}">
          {{$version_list[''.$param['version_id']]}}
        </td>
      </tr>
      <tr><th>備考</th><td><input type="hidden" name="memo" value="{{$param['memo']}}">{{$param['memo']}}</td></tr>
    </table>
    <input type="submit" value="送信">
    </form>
    <p><a href="{{ route('admin.site.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
