<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サイト情報一覧</b></p>
    </div>
    
    <form action="{{ route('admin.site.add') }}" method="post">
    @csrf
    <input type="submit" value="サイト情報追加">
    </form>
    
    <table border="2">
      <tr><th>id</th><th>サイト名</th><th>バージョン</th><th>備考</th><th>修正</th><th>削除</th></tr>
    @for ($i = 0; $i < count($list); $i++)
      <tr>
      <td>{{$list[$i]['id']}}</td>
      <td>{{$list[$i]['site_name']}}</td>
      <td>{{$version_list[$list[$i]['version_id']]}}</td>
      <td>{{$list[$i]['memo']}}</td>
      <td><form action="{{ route('admin.site.upd', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="修正"></form></td>
      <td>
      @if ($list[$i]['site_delete_ok'] === true)
        <form action="{{ route('admin.site.delcfm', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="削除"></form>
      @else
        -
      @endif
      </td>
      </tr>
    @endfor
    </table>
    <p><a href="{{ route('admin.menu') }}">メニューへ戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
