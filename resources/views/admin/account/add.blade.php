<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム AWARDアカウント追加</b></p>
    </div>
    
    @for ($i = 0; $i < count($msg); $i++)
      <p><font color="red">{{$msg[$i]}}</font></p>
    @endfor
    
    <form action="{{ route('admin.account.addcfm', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>アカウント名</th><td><input type="text" name="name" value="{{$param['name']}}"></td></tr>
      <tr><th>EMAIL</th><td><input type="text" name="email" value="{{$param['email']}}"></td></tr>
      <tr><th>パスワード</th><td><input type="text" name="password" value="{{$param['password']}}"></td></tr>
      <tr><th>事務局名称(日本語)</th><td><input type="text" name="secretariat_name_jp" value="{{$param['secretariat_name_jp']}}"></td></tr>
      <tr><th>事務局名称(英語)</th><td><input type="text" name="secretariat_name_en" value="{{$param['secretariat_name_en']}}"></td></tr>
      <tr><th>事務局郵便番号</th><td><input type="text" name="secretariat_postalcode" value="{{$param['secretariat_postalcode']}}"></td></tr>
      <tr><th>事務局住所(日本語)</th><td><input type="text" name="secretariat_staddress_jp" value="{{$param['secretariat_staddress_jp']}}"></td></tr>
      <tr><th>事務局住所(英語)</th><td><input type="text" name="secretariat_staddress_en" value="{{$param['secretariat_staddress_en']}}"></td></tr>
      <tr><th>事務局電話番号(日本表記)</th><td><input type="text" name="secretariat_tel_jp" value="{{$param['secretariat_tel_jp']}}"></td></tr>
      <tr><th>事務局電話番号(国際表記)</th><td><input type="text" name="secretariat_tel_en" value="{{$param['secretariat_tel_en']}}"></td></tr>
      <tr><th>事務局メールアドレス</th><td><input type="text" name="secretariat_mail" value="{{$param['secretariat_mail']}}"></td></tr>
      <tr><th>事務局担当者名</th><td><input type="text" name="secretariat_staff_name" value="{{$param['secretariat_staff_name']}}"></td></tr>
      <tr><th>事務局担当者部署</th><td><input type="text" name="secretariat_staff_department" value="{{$param['secretariat_staff_department']}}"></td></tr>
      <tr><th>備考</th><td><textarea name="memo" rows="10">{{$param['memo']}}</textarea></td></tr>
    </table>
    <input type="submit" value="確認">
    </form>
    <p><a href="{{ route('admin.account.list') }}">戻る</a></p>

</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
