<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム AWARDアカウント修正確認</b></p>
    </div>
    
    <p><font color="red">AWARDアカウントを修正しますか</font></p>
    
    <form action="{{ route('admin.account.updcmp', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>id</th><td><input type="hidden" name="id" value="{{$param['id']}}">{{$param['id']}}</td></tr>
      <tr><th>アカウント名</th><td><input type="hidden" name="name" value="{{$param['name']}}">{{$param['name']}}</td></tr>
      <tr><th>EMAIL</th><td><input type="hidden" name="email" value="{{$param['email']}}">{{$param['email']}}</td></tr>
      <tr><th>パスワード</th><td><input type="hidden" name="password" value="{{$param['password']}}">******</td></tr>
      <tr><th>事務局名称(日本語)</th><td><input type="hidden" name="secretariat_name_jp" value="{{$param['secretariat_name_jp']}}">{{$param['secretariat_name_jp']}}</td></tr>
      <tr><th>事務局名称(英語)</th><td><input type="hidden" name="secretariat_name_en" value="{{$param['secretariat_name_en']}}">{{$param['secretariat_name_en']}}</td></tr>
      <tr><th>事務局郵便番号</th><td><input type="hidden" name="secretariat_postalcode" value="{{$param['secretariat_postalcode']}}">{{$param['secretariat_postalcode']}}</td></tr>
      <tr><th>事務局住所(日本語)</th><td><input type="hidden" name="secretariat_staddress_jp" value="{{$param['secretariat_staddress_jp']}}">{{$param['secretariat_staddress_jp']}}</td></tr>
      <tr><th>事務局住所(英語)</th><td><input type="hidden" name="secretariat_staddress_en" value="{{$param['secretariat_staddress_en']}}">{{$param['secretariat_staddress_en']}}</td></tr>
      <tr><th>事務局電話番号(日本表記)</th><td><input type="hidden" name="secretariat_tel_jp" value="{{$param['secretariat_tel_jp']}}">{{$param['secretariat_tel_jp']}}</td></tr>
      <tr><th>事務局電話番号(国際表記)</th><td><input type="hidden" name="secretariat_tel_en" value="{{$param['secretariat_tel_en']}}">{{$param['secretariat_tel_en']}}</td></tr>
      <tr><th>事務局メールアドレス</th><td><input type="hidden" name="secretariat_mail" value="{{$param['secretariat_mail']}}">{{$param['secretariat_mail']}}</td></tr>
      <tr><th>事務局担当者名</th><td><input type="hidden" name="secretariat_staff_name" value="{{$param['secretariat_staff_name']}}">{{$param['secretariat_staff_name']}}</td></tr>
      <tr><th>事務局担当者部署</th><td><input type="hidden" name="secretariat_staff_department" value="{{$param['secretariat_staff_department']}}">{{$param['secretariat_staff_department']}}</td></tr>
      <tr><th>備考</th><td><input type="hidden" name="memo" value="{{$param['memo']}}">{{$param['memo']}}</td></tr>
    </table>
    <input type="submit" value="送信">
    </form>
    <p><a href="{{ route('admin.account.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
