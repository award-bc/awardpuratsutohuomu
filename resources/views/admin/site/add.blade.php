<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サイト情報追加</b></p>
    </div>
    
    @for ($i = 0; $i < count($msg); $i++)
      <p><font color="red">{{$msg[$i]}}</font></p>
    @endfor
    
    <form action="{{ route('admin.site.addcfm', $param['id']) }}" method="post">
    @csrf
    <table border="2">
      <tr><th>サイト名(日本語表記)</th><td><input type="text" name="site_name" value="{{$param['site_name']}}"></td></tr>
      <tr><th>URL</th><td>https://(ドメイン名)/<input type="text" name="path_name" value="{{$param['path_name']}}">/</td></tr>
      <tr>
        <th>AWARDアカウント</th>
        <td>
          <select name="account_id">
          @foreach ($account_list as $vid => $vname)
            <option value="{{$vid}}" @if (''.$param['account_id'] == ''.$vid) selected @endif >{{$vname}}</option>
          @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>WEBサーバー</th>
        <td>
          <select name="web_server_id">
          @foreach ($web_server_list as $vid => $vname)
            <option value="{{$vid}}" @if (''.$param['web_server_id'] == ''.$vid) selected @endif >{{$vname}}</option>
          @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>DBサーバー</th>
        <td>
          <select name="db_server_id">
          @foreach ($db_server_list as $vid => $vname)
            <option value="{{$vid}}" @if (''.$param['db_server_id'] == ''.$vid) selected @endif >{{$vname}}</option>
          @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>バージョン</th>
        <td>
          <select name="version_id">
          @foreach ($version_list as $vid => $vname)
            <option value="{{$vid}}" @if (''.$param['version_id'] == ''.$vid) selected @endif >{{$vname}}</option>
          @endforeach
          </select>
        </td>
      </tr>
      <tr><th>備考</th><td><textarea name="memo" rows="10">{{$param['memo']}}</textarea></td></tr>
    </table>
    <input type="submit" value="確認">
    </form>
    <p><a href="{{ route('admin.site.list') }}">戻る</a></p>

</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
