<?php

namespace App\DBManager;

use Illuminate\Database\Eloquent\Model;
use App\Models\Site;
use Illuminate\Support\Facades\Hash;

class SiteManager extends CommonDBManager
{
    // モデルを返却
    // ＊親クラスの仮想関数の実態
    protected function getModel() {
        return new \App\Models\Site;
    }
    
    // 曖昧検索をするキーのリストを返却
    // ＊親クラスの仮想関数の実態
    protected function getLikeList() {
        return array(
            'site_name',
            'memo',
        );
    }
    protected function getEncryptList() {
        return array();
    }
    
}
