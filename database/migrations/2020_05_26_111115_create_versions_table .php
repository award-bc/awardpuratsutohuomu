<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versions', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('バージョンID');
            $table->string('version_name', 191)->comment('バージョン名(半角英数)');
            $table->bigInteger('base_version_id')->comment('ベースとなった前バージョンのバージョンID');
            $table->tinyInteger('release_type')->comment('リリース種別(0:開発中 1:リリース)')->default(0);
            $table->tinyInteger('custom_type')->comment('カスタム種別(0:一般 1:カスタム)')->default(0);
            $table->longText('memo')->comment('備考(追加機能等の説明)')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('versions');
    }
}
