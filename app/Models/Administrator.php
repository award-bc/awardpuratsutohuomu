<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Administrator extends Model
{
    use SoftDeletes;
    
    protected $table = 'administrators';
    protected $guarded = array('id');
    public $timestamps = true;
    
}
