<?php

namespace App\DBManager;

use Illuminate\Database\Eloquent\Model;
use App\Models\Version;
use Illuminate\Support\Facades\Hash;

class VersionManager extends CommonDBManager
{
    // モデルを返却
    // ＊親クラスの仮想関数の実態
    protected function getModel() {
        return new \App\Models\Version;
    }
    
    // 曖昧検索をするキーのリストを返却
    // ＊親クラスの仮想関数の実態
    protected function getLikeList() {
        return array(
            'version_name',
            'memo',
        );
    }
    protected function getEncryptList() {
        return array();
    }
    
}
