<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム 管理者追加</b></p>
    </div>
    
    @for ($i = 0; $i < count($msg); $i++)
      <p><font color="red">{{$msg[$i]}}</font></p>
    @endfor
    
    <form action="{{ route('admin.administrator.addcfm') }}" method="post">
    @csrf
    <table border="2">
      <tr><th>アカウント名</th><th>EMAIL</th><th>パスワード</th><th>備考</th></tr>
      <tr>
      <td><input type="text" name="name" value="{{$param['name']}}"></td>
      <td><input type="text" name="email" value="{{$param['email']}}"></td>
      <td><input type="text" name="password" value="{{$param['password']}}"></td>
      <td><textarea name="memo" rows="10">{{$param['memo']}}</textarea></td>
      </tr>
    </table>
    <input type="submit" value="確認">
    </form>
    <p><a href="{{ route('admin.administrator.list') }}">戻る</a></p>

</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
