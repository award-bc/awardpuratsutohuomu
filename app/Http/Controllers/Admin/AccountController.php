<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\DBManager\AccountManager;
use App\Library\AdminCommon;
use Illuminate\Support\Facades\Schema;


class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:administrator');
    }
    
    /**
     * Show the application dashboard.
     *
     */
    public function list(Request $request) {
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'list' => array(),
        );
        
        $dbm = new AccountManager();
        if ($dbm->select() == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        $page_data['list'] = $dbm->getRows();
        
        // ページ表示
        return view('admin.account.list', $page_data);
    }
    
    public function add(Request $request) {
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // ページ表示
        return view('admin.account.add', $page_data);
    }
    
    public function addcfm(Request $request) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // バリデーション
        $validation_result = $admin->validationCheck($page_data['param'], Schema::getColumnListing('accounts'));
        if ($validation_result !== true) {
            if (is_array($validation_result)) {
                foreach($validation_result as $key => $value) {
                    array_push($page_data['msg'], '[' . $key . ']' . $value);
                }
            } else {
                array_push($page_data['msg'], 'エラーが発生しました');
            }
            
            // バリデートエラー(エラーメッセージ付でadd表示)
            return view('admin.account.add', $page_data);
            //return redirect()->action('Admin\AccountController@add', $page_data);
        }
        
        // ページ表示
        return view('admin.account.addcfm', $page_data);
    }
    
    public function addcmp(Request $request) {
        // DBマネージャー
        $db = new AccountManager();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // データ追加
        if ($db->insert_with_exparm($page_data['param'], true) !== true) {
            // エラー
            redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.account.addcmp', $page_data);
    }
    
    public function delcfm(Request $request, String $id) {
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new AccountManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // 取得した値をセット
        $tmp_rows = $dbm->getRows();
        $page_data['param'] = $tmp_rows[0];
        
        // ページ表示
        return view('admin.account.delcfm', $page_data);
    }
    
    public function delcmp(Request $request, String $id) {
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new AccountManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // 値を削除
        if ($dbm->delete($id) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.account.delcmp', $page_data);
    }
    
    public function upd(Request $request, String $id) {
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        if ($page_data['param']['id'] === null) {
            // パラメータの入力が無かった場合はDBより値を取得
            // 存在チェック
            $dbm = new AccountManager();
            if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
                // システムエラー
                return redirect()->action('Admin\ErrController@index');
            } else if ($dbm->getAllCount() == 0) {
                // システムエラー
                return redirect()->action('Admin\ErrController@index');
            }
            
            // 取得した値をセット
            $tmp_rows = $dbm->getRows();
            $page_data['param'] = $tmp_rows[0];
        }
        
        // ページ表示
        return view('admin.account.upd', $page_data);
    }
    
    public function updcfm(Request $request, String $id) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new AccountManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ('' . $id != '' . $page_data['param']['id']) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バリデーション
        $validation_result = $admin->validationCheck($page_data['param'], Schema::getColumnListing('accounts'));
        if ($validation_result !== true) {
            if (is_array($validation_result)) {
                foreach($validation_result as $key => $value) {
                    array_push($page_data['msg'], '[' . $key . ']' . $value);
                }
            } else {
                array_push($page_data['msg'], 'エラーが発生しました');
            }
            
            // バリデートエラー(エラーメッセージ付でupd表示)
            return view('admin.account.upd', $page_data);
            //return redirect()->action('Admin\AccountController@upd', $page_data);
        }
        
        // ページ表示
        return view('admin.account.updcfm', $page_data);
    }
    
    public function updcmp(Request $request, String $id) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('accounts'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new AccountManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ('' . $id != '' . $page_data['param']['id']) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // データ更新
        $dbm->clear();
        if ($dbm->update_with_exparm($page_data['param'], true) !== true) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.account.updcmp', $page_data);
    }
    
    private function create_empty_param_array($table_name) {
        $table_columns = Schema::getColumnListing($table_name);
        $empty_data = array();
        
        foreach($table_columns as $value) {
            $empty_data[$value] = null;
        }
        
        return $empty_data;
    }
    
}
