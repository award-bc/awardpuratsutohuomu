<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サーバー情報一覧</b></p>
    </div>
    
    <form action="{{ route('admin.server.add') }}" method="post">
    @csrf
    <input type="submit" value="サーバー情報追加">
    </form>
    
    <table border="2">
      <tr><th>id</th><th>サーバー名(日本語表記)</th><th>ホスト</th><th>サーバー種別</th><th>備考</th><th>修正</th><th>削除</th></tr>
    @for ($i = 0; $i < count($list); $i++)
      <tr>
      <td>{{$list[$i]['id']}}</td>
      <td>{{$list[$i]['server_name']}}</td>
      <td>{{$list[$i]['host']}}</td>
      <td>
        @if (''.$list[$i]['service_code'] == '0')
          webサーバー
        @else
          DBサーバー
        @endif
      </td>
      <td>{{$list[$i]['memo']}}</td>
      <td><form action="{{ route('admin.server.upd', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="修正"></form></td>
      <td><form action="{{ route('admin.server.delcfm', $list[$i]['id']) }}" method="post">@csrf<input type="submit" value="削除"></form></td>
      </tr>
    @endfor
    </table>
    <p><a href="{{ route('admin.menu') }}">メニューへ戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
