<?php

namespace App\DBManager;

use Illuminate\Database\Eloquent\Model;
use App\Models\Server;
use Illuminate\Support\Facades\Hash;

class ServerManager extends CommonDBManager
{
    // モデルを返却
    // ＊親クラスの仮想関数の実態
    protected function getModel() {
        return new \App\Models\Server;
    }
    
    // 曖昧検索をするキーのリストを返却
    // ＊親クラスの仮想関数の実態
    protected function getLikeList() {
        return array(
            'host',
            'db_server',
            'memo',
        );
    }
    protected function getEncryptList() {
        return array(
            'host_root_password',
            'db_root_password',
        );
    }
    
}
