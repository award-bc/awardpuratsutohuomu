<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム サイト情報修正完了</b></p>
    </div>
    
    <p><font color="red">サイト情報を修正しました</font></p>
    
    <p><a href="{{ route('admin.site.list') }}">戻る</a></p>
    
</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
