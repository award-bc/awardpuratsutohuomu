<!-- resources/views/layouts/admin.blade.phpを継承 -->
@extends('layouts.admin')

@section('content')
<div class="container">

  <div class="qaWrap">
    <div class="outlineText">
      <p><b>AWARDオンラインプラットフォーム MENU</b></p>
    </div>

    <p><a href="{{ route('admin.administrator.list') }}">1.管理ユーザー一覧</a></p>
    <p><a href="{{ route('admin.server.list') }}">2.サーバー情報一覧</a></p>
    <p><a href="{{ route('admin.account.list') }}">3.AWARDアカウント一覧</a></p>
    <p><a href="{{ route('admin.version.list') }}">4.バージョン情一覧</a></p>
    <p><a href="{{ route('admin.site.list') }}">5.サイト情報一覧</a></p>

</div>
<!-- // qaWrap -->

</div>
<!-- // container -->
@endsection
