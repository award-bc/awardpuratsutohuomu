<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Library\AdminCommon;
use App\Library\SiteOperation;
use Illuminate\Support\Facades\Schema;
use App\DBManager\AccountManager;
use App\DBManager\ServerManager;
use App\DBManager\VersionManager;
use App\DBManager\SiteManager;


class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:administrator');
    }
    
    /**
     * Show the application dashboard.
     *
     */
    public function list(Request $request) {
        // AWARDアカウントリストを取得
        $account_list = $this->get_account_list();
        if ($account_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // WEBサーバーリストを取得
        $web_server_list = $this->get_web_server_list();
        if ($web_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // DBサーバーリストを取得
        $db_server_list = $this->get_db_server_list();
        if ($db_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バージョンリストを取得
        $version_list = $this->get_version_list();
        if ($version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'list' => array(),
            'account_list'    => $account_list,
            'web_server_list' => $web_server_list,
            'db_server_list'  => $db_server_list,
            'version_list'    => $version_list,
        );
        
        $dbm = new SiteManager();
        if ($dbm->select() == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        $page_data['list'] = $dbm->getRows();
        
        // 削除可能かどうかを追記
        for($i=0; $i < count($page_data['list']); $i++) {
            $db_server_info = $this->get_server_info($page_data['list'][$i]['db_server_id']);
            $version_info   = $this->get_version_info($page_data['list'][$i]['version_id']);
            
            $site_operation = new SiteOperation();
            $site_operation->set_db_setting($page_data['list'][$i]['path_name'] . '_' . $version_info['version_name'], $db_server_info['host'], 'root', $db_server_info['root_password']);
            $site_delete_ok = $site_operation->get_db_site_settings('SITE_DELETE_OK');
            if ($site_delete_ok === false) {
            	$page_data['list'][$i]['site_delete_ok'] = true;
            } else if (!isset($site_delete_ok['value_en'])) {
            	$page_data['list'][$i]['site_delete_ok'] = true;
            } else if (''.$site_delete_ok['value_en'] == '0'){
            	$page_data['list'][$i]['site_delete_ok'] = false;
            } else {
            	$page_data['list'][$i]['site_delete_ok'] = true;
            }
        }
        
        // ページ表示
        return view('admin.site.list', $page_data);
    }
    
    public function add(Request $request) {
        // AWARDアカウントリストを取得
        $account_list = $this->get_account_list();
        if ($account_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // WEBサーバーリストを取得
        $web_server_list = $this->get_web_server_list();
        if ($web_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // DBサーバーリストを取得
        $db_server_list = $this->get_db_server_list();
        if ($db_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バージョンリストを取得
        $version_list = $this->get_version_list();
        if ($version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
            'account_list'    => $account_list,
            'web_server_list' => $web_server_list,
            'db_server_list'  => $db_server_list,
            'version_list'    => $version_list,
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // ページ表示
        return view('admin.site.add', $page_data);
    }
    
    public function addcfm(Request $request) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // AWARDアカウントリストを取得
        $account_list = $this->get_account_list();
        if ($account_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // WEBサーバーリストを取得
        $web_server_list = $this->get_web_server_list();
        if ($web_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // DBサーバーリストを取得
        $db_server_list = $this->get_db_server_list();
        if ($db_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バージョンリストを取得
        $version_list = $this->get_version_list();
        if ($version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
            'account_list'    => $account_list,
            'web_server_list' => $web_server_list,
            'db_server_list'  => $db_server_list,
            'version_list'    => $version_list,
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // バリデーション
        $validation_result = $admin->validationCheck($page_data['param'], Schema::getColumnListing('sites'));
        if ($validation_result !== true) {
            if (is_array($validation_result)) {
                foreach($validation_result as $key => $value) {
                    array_push($page_data['msg'], '[' . $key . ']' . $value);
                }
            } else {
                array_push($page_data['msg'], 'エラーが発生しました');
            }
            
            // バリデートエラー(エラーメッセージ付でadd表示)
            return view('admin.site.add', $page_data);
            //return redirect()->action('Admin\SiteController@add', $page_data);
        }
        
        // ページ表示
        return view('admin.site.addcfm', $page_data);
    }
    
    public function addcmp(Request $request) {
        // DBマネージャー
        $db = new SiteManager();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // プラットフォーム内を更新する前に対象サーバーに実態を作成
        $account_info = $this->get_account_info($page_data['param']['account_id']);
        $web_server_info = $this->get_server_info($page_data['param']['web_server_id']);
        $db_server_info = $this->get_server_info($page_data['param']['db_server_id']);
        $version_info = $this->get_version_info($page_data['param']['version_id']);
        
        //// 暫定対処(同サーバーとみなしてサーバー内のファイルコピー)
        //exec('cp -a '. $web_server_info['web_master_path'] . '/' . $version_info['version_name'] . ' ' . $web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name']);
        //exec('ln -s '. $web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name'] . '/public_html ' . $web_server_info['web_document_root_path'] . '/link/' . $page_data['param']['path_name']);
        
        //if (copy(
        //    $web_server_info['web_master_path'] . '/' . $version_info['version_name'], 
        //    $web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name']
        //    )) {
        //    // エラー
        //    redirect()->action('Admin\ErrController@index');
        //}
        
        //// リンク作成
        //if (link(
        //    $web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name'] . '/public_html', 
        //    $web_server_info['web_document_root_path'] . '/link/' . $page_data['param']['path_name']
        //    )) {
        //    // エラー
        //    redirect()->action('Admin\ErrController@index');
        //}
        
        $site_operation = new SiteOperation();
        
        // サイトの作成
        // サイトデータのコピー及びドキュメントルートにlink作成
        $site_operation->copy_dir($web_server_info['web_master_path'] . '/' . $version_info['version_name'], $web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name']);
        $site_operation->make_link($web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name'] . '/public_html', $web_server_info['web_document_root_path'] . '/link/' . $page_data['param']['path_name']);
        
        // サイトデータのCONFIG_ENV.phpファイルの置換
        $replacement_arr = array(
            '----------MYSQL_USERNAME----------' => 'root',
            '----------MYSQL_PASSWORD----------' => $db_server_info['root_password'],
            '----------MYSQL_HOST----------'     => $db_server_info['host'],
            '----------MYSQL_DBNAME----------'   => $page_data['param']['path_name'] . '_' . $version_info['version_name'],
        );
        $site_operation->replacement_file($web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name'] . '/configs/CONFIG_ENV.php', $replacement_arr);
        
        // DB作成
        $site_operation->set_db_setting($page_data['param']['path_name'] . '_' . $version_info['version_name'], $db_server_info['host'], 'root', $db_server_info['root_password']);
        if ($site_operation->create_db() === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        // DB初期化
        if ($site_operation->file_exec($web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name'] . '/' . 'init.sql') === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        // カスタマイズデータ変更
        if ($site_operation->set_db_site_settings('ORGANIZATION_NAME', $account_info['secretariat_name_en'], $account_info['secretariat_name_jp']) === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        if ($site_operation->set_db_site_settings('ORGANIZATION_POSTAL_CODE', $account_info['secretariat_postalcode'], $account_info['secretariat_postalcode']) === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        if ($site_operation->set_db_site_settings('ORGANIZATION_ADDRESS', $account_info['secretariat_staddress_en'], $account_info['secretariat_staddress_jp']) === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        if ($site_operation->set_db_site_settings('ORGANIZATION_MAIL', $account_info['secretariat_mail'], $account_info['secretariat_mail']) === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        if ($site_operation->set_db_site_settings('ORGANIZATION_MAIL', $account_info['secretariat_mail'], $account_info['secretariat_mail']) === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // データ追加
        if ($db->insert($page_data['param'], true) !== true) {
            // エラー
            redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.site.addcmp', $page_data);
    }
    
    public function delcfm(Request $request, String $id) {
        // AWARDアカウントリストを取得
        $account_list = $this->get_account_list();
        if ($account_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // WEBサーバーリストを取得
        $web_server_list = $this->get_web_server_list();
        if ($web_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // DBサーバーリストを取得
        $db_server_list = $this->get_db_server_list();
        if ($db_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バージョンリストを取得
        $version_list = $this->get_version_list();
        if ($version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
            'account_list'    => $account_list,
            'web_server_list' => $web_server_list,
            'db_server_list'  => $db_server_list,
            'version_list'    => $version_list,
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new SiteManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // 取得した値をセット
        $tmp_rows = $dbm->getRows();
        $page_data['param'] = $tmp_rows[0];
        
        // ページ表示
        return view('admin.site.delcfm', $page_data);
    }
    
    public function delcmp(Request $request, String $id) {
        $site_operation = new SiteOperation();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // プラットフォーム内を更新する前に対象サーバーの実態を削除
        $web_server_info = $this->get_server_info($page_data['param']['web_server_id']);
        $db_server_info = $this->get_server_info($page_data['param']['db_server_id']);
        $version_info = $this->get_version_info($page_data['param']['version_id']);
        
        // サイトの削除
        $site_operation->del_dir($web_server_info['web_document_root_path'] . '/app/' . $page_data['param']['path_name'] . '_' . $version_info['version_name']);
        $site_operation->del_link($web_server_info['web_document_root_path'] . '/link/' . $page_data['param']['path_name']);
        
        // DB削除
        $site_operation->set_db_setting($page_data['param']['path_name'] . '_' . $version_info['version_name'], $db_server_info['host'], 'root', $db_server_info['root_password']);
        if ($site_operation->drop_db() === false) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // 存在チェック
        $dbm = new SiteManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // 値を削除
        if ($dbm->delete($id) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.site.delcmp', $page_data);
    }
    
    public function upd(Request $request, String $id) {
        // AWARDアカウントリストを取得
        $account_list = $this->get_account_list();
        if ($account_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // WEBサーバーリストを取得
        $web_server_list = $this->get_web_server_list();
        if ($web_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // DBサーバーリストを取得
        $db_server_list = $this->get_db_server_list();
        if ($db_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バージョンリストを取得
        $version_list = $this->get_version_list();
        if ($version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
            'account_list'    => $account_list,
            'web_server_list' => $web_server_list,
            'db_server_list'  => $db_server_list,
            'version_list'    => $version_list,
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        if ($page_data['param']['id'] === null) {
            // パラメータの入力が無かった場合はDBより値を取得
            // 存在チェック
            $dbm = new SiteManager();
            if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
                // システムエラー
                return redirect()->action('Admin\ErrController@index');
            } else if ($dbm->getAllCount() == 0) {
                // システムエラー
                return redirect()->action('Admin\ErrController@index');
            }
            
            // 取得した値をセット
            $tmp_rows = $dbm->getRows();
            $page_data['param'] = $tmp_rows[0];
        }
        
        // ページ表示
        return view('admin.site.upd', $page_data);
    }
    
    public function updcfm(Request $request, String $id) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // AWARDアカウントリストを取得
        $account_list = $this->get_account_list();
        if ($account_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // WEBサーバーリストを取得
        $web_server_list = $this->get_web_server_list();
        if ($web_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // DBサーバーリストを取得
        $db_server_list = $this->get_db_server_list();
        if ($db_server_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バージョンリストを取得
        $version_list = $this->get_version_list();
        if ($version_list === false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
            'account_list'    => $account_list,
            'web_server_list' => $web_server_list,
            'db_server_list'  => $db_server_list,
            'version_list'    => $version_list,
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new SiteManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ('' . $id != '' . $page_data['param']['id']) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // バリデーション
        $validation_result = $admin->validationCheck($page_data['param'], Schema::getColumnListing('sites'));
        if ($validation_result !== true) {
            if (is_array($validation_result)) {
                foreach($validation_result as $key => $value) {
                    array_push($page_data['msg'], '[' . $key . ']' . $value);
                }
            } else {
                array_push($page_data['msg'], 'エラーが発生しました');
            }
            
            // バリデートエラー(エラーメッセージ付でupd表示)
            return view('admin.site.upd', $page_data);
            //return redirect()->action('Admin\SiteController@upd', $page_data);
        }
        
        // ページ表示
        return view('admin.site.updcfm', $page_data);
    }
    
    public function updcmp(Request $request, String $id) {
        // 共通クラス
        $admin = new AdminCommon();
        
        // テンプレートに渡す配列
        $page_data = array(
            'msg' => array(),
            'param' => $this->create_empty_param_array('sites'),
        );
        
        // 入力値をセット
        foreach ($page_data['param'] as $key => $value) {
            $page_data['param'][$key] = $request->input($key, null);
        }
        
        // 存在チェック
        $dbm = new SiteManager();
        if ($dbm->select(0, 1, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ($dbm->getAllCount() == 0) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        } else if ('' . $id != '' . $page_data['param']['id']) {
            // システムエラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // データ更新
        $dbm->clear();
        if ($dbm->update($page_data['param'], true) !== true) {
            // エラー
            return redirect()->action('Admin\ErrController@index');
        }
        
        // ページ表示
        return view('admin.site.updcmp', $page_data);
    }
    
    private function create_empty_param_array($table_name) {
        $table_columns = Schema::getColumnListing($table_name);
        $empty_data = array();
        
        foreach($table_columns as $value) {
            $empty_data[$value] = null;
        }
        
        return $empty_data;
    }
    
    private function get_account_list() {
        // DBマネージャー
        $db = new AccountManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select() == false) {
            // システムエラー
            return false;
        }
        $return_list = array();
        //$return_list = array('0' => '-');
        $tmp_rows = $db->getRows();
        $db->clear();
        foreach ($tmp_rows as $row) {
            $return_list[''.$row['id']] = $row['name'];
        }
        
        return $return_list;
    }
    
    private function get_web_server_list() {
        // DBマネージャー
        $db = new ServerManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select(NULL, NULL, NULL, NULL, array('service_code' => '0')) == false) {
            // システムエラー
            return false;
        }
        $return_list = array();
        //$return_list = array('0' => '-');
        $tmp_rows = $db->getRows();
        $db->clear();
        foreach ($tmp_rows as $row) {
            $return_list[''.$row['id']] = $row['host'];
        }
        
        return $return_list;
    }
    
    private function get_db_server_list() {
        // DBマネージャー
        $db = new ServerManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select(NULL, NULL, NULL, NULL, array('service_code' => '1')) == false) {
            // システムエラー
            return false;
        }
        $return_list = array();
        //$return_list = array('0' => '-');
        $tmp_rows = $db->getRows();
        $db->clear();
        foreach ($tmp_rows as $row) {
            $return_list[''.$row['id']] = $row['host'];
        }
        
        return $return_list;
    }
    
    private function get_version_list() {
        // DBマネージャー
        $db = new VersionManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select() == false) {
            // システムエラー
            return false;
        }
        $return_list = array();
        //$return_list = array('0' => '-');
        $tmp_rows = $db->getRows();
        $db->clear();
        foreach ($tmp_rows as $row) {
            $return_list[''.$row['id']] = $row['version_name'];
        }
        
        return $return_list;
    }
    
    private function get_account_info($id) {
        // DBマネージャー
        $db = new AccountManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select(NULL, NULL, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return false;
        }
        $return_info = $db->getRows();
        $db->clear();
        
        return $return_info[0];
    }
    
    private function get_server_info($id) {
        // DBマネージャー
        $db = new ServerManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select(NULL, NULL, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return false;
        }
        $return_info = $db->getRows();
        $db->clear();
        
        return $return_info[0];
    }
    
    private function get_version_info($id) {
        // DBマネージャー
        $db = new VersionManager();
        
        // 現在のバージョン情報の一覧を取得
        if ($db->select(NULL, NULL, NULL, NULL, array('id' => $id)) == false) {
            // システムエラー
            return false;
        }
        $return_info = $db->getRows();
        $db->clear();
        
        return $return_info[0];
    }
    
}
