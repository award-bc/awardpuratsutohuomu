<?php

namespace App\DBManager;

use DB;
use Illuminate\Support\Facades\Schema;

// DBマネージャーの共通機能
abstract class CommonDBManager
{
    // 全体の検索ヒット数
    private $allcount = 0;
    
    // 検索レコード格納
    private $resultdata = array();
    
    // 例外データ
    private $exception = null;
    
    // 抽象クラス定義(子クラスで実装)
    // テーブルモデルを返却
    abstract protected function getModel();
    // 曖昧検索をするキーのリストを返却
    abstract protected function getLikeList();
    // 暗号化するキーのリストを返却
    abstract protected function getEncryptList();
    
    // トランザクション開始
    public static function beginTransaction() {
        DB::beginTransaction();
        return;
    }
    
    // コミット
    public static function commit() {
        DB::commit();
        return;
    }
    
    // ロールバック
    public static function rollback() {
        DB::rollback();
        return;
    }
    
    // テーブルのレコード追加
    public function insert(array $insert_row = array(), bool $commit = true) {
        // トランザクション開始
        if ($commit) self::beginTransaction();
        
        try {
            $model = $this->getModel();
            
            // データを追加
            // 暗号化
            $tmp_row = $this->squeeze_columns($insert_row, $model);
            // insertなのでcreated_atを挿入
            if (array_key_exists('created_at', $tmp_row)) {
                $tmp_row['created_at'] = date('Y-m-d H:i:s');
            }
            foreach ($insert_row as $key => $val) {
                if (in_array($key, $this->getEncryptList())) {
                    $tmp_row[$key] = $this->encrypt($val);
                }
            }
            $model->fill($tmp_row);
            $model->save();
            
            $this->allcount = 1;
            $this->resultdata = array($model->toArray());
            
            // コミット
            if ($commit) self::commit();
            
        } catch(Exception $e) {
            // 例外発生
            $this->exception = $e;
            if ($commit) self::rollback();
            return false;
        }
        
        return true;
    }
    
    // テーブルのレコード変更
    public function update(array $update_row = array(), bool $commit = true) {
        
        // トランザクション開始
        if ($commit) self::beginTransaction();
        
        try {
            $model = $this->getModel();
            
            // 変更レコードを取得
            $update_model = $model->where('id', $update_row['id'])->get()->first();
            // データセット
            $tmp_row = $this->squeeze_columns($update_row, $model);
            // updateなのでcreated_atは除外
            if (array_key_exists('created_at', $tmp_row)) {
                unset($tmp_row['created_at']);
            }
            // updateなのでupdated_atを更新
            if (array_key_exists('updated_at', $tmp_row)) {
                $tmp_row['updated_at'] = date('Y-m-d H:i:s');
            }
            foreach ($tmp_row as $key => $value) {
                // 暗号化
                if (in_array($key, $this->getEncryptList())) {
                    $update_model[$key] = $this->encrypt($value);
                } else {
                    $update_model[$key] = $value;
                }
            }
            
            // 変更を保存
            $update_model->save();
            
            $this->allcount = 1;
            $this->resultdata = array($update_model->toArray());
            
            // コミット
            if ($commit) self::commit();
            
        } catch(Exception $e) {
            // 例外発生
            $this->exception = $e;
            if ($commit) self::rollback();
            return false;
        }
        
        return true;
    }
    
    // テーブルのレコード削除
    public function delete(int $id, bool $commit = true) {
        
        // トランザクション開始
        if ($commit) self::beginTransaction();
        
        try {
            $model = $this->getModel();
            
            // 変更レコードを取得
            $model->destroy($id);
            
            // コミット
            if ($commit) self::commit();
            
        } catch(Exception $e) {
            // 例外発生
            $this->exception = $e;
            if ($commit) self::rollback();
            return false;
        }
        
        return true;
    }
    
    // テーブルの検索
    public function select(int $start_index=NULL, int $max=NULL, string $sort_key=NULL, string $sort=NULL, array $where = array()) {
        try {
            $new_model = $this->getModel();
            $model = NULL;
            
            // WHERE設定
            foreach ($where as $key => $value) {
                if ($value != NULL) {
                    if (in_array($key, $this->getLikeList())) {
                        // 曖昧検索
                        //$model->where($key, 'like', $value);
                        if ($model === NULL) {
                            $model = $new_model::where($key, 'like', $value);
                        } else {
                            $model->where($key, 'like', $value);
                        }
                    } else {
                        //$model->where($key, $value);
                        if ($model === NULL) {
                            $model = $new_model::where($key, $value);
                        } else {
                            $model->where($key, $value);
                        }
                    }
                }
            }
            
            // 全体のヒット数を取得
            if ($model === NULL) {
                $this->allcount = $new_model->count();
            } else {
                $this->allcount = $model->count();
            }
            
            
            // ORDER BY設定
            if ($sort_key != NULL) {
                if ($sort == 'DESC') {
                    //$model->orderBy($sort_key, 'desc');
                    if ($model === NULL) {
                        $model = $new_model::orderBy($sort_key, 'desc');
                    } else {
                        $model->orderBy($sort_key, 'desc');
                    }
                } else {
                    //$model->orderBy($sort_key, 'asc');
                    if ($model === NULL) {
                        $model = $new_model::orderBy($sort_key, 'asc');
                    } else {
                        $model->orderBy($sort_key, 'asc');
                    }
                }
            }
            
            // limit設定
            if ($start_index != NULL) {
                //$model->offset($start_index);
                if ($model === NULL) {
                    $model = $new_model::skip($start_index);
                } else {
                    $model->skip($start_index);
                }
            }
            if ($max != NULL) {
                //$model->limit($max);
                if ($model === NULL) {
                    $model = $new_model::take($max);
                } else {
                    $model->take($max);
                }
            }
            
            if ($model === NULL) {
                $model = $new_model;
            }
            
//echo $model->toSql();
            // データを取得
            if ($this->allcount > 0) {
                /*
                $this->resultdata = $model->get()->all()->toArray();
                //$this->resultdata = $model->get()->all();
                */
                $all_collection = $model->get()->all();
                $this->resultdata = array();
                foreach ($all_collection as $row) {
                    // 複合化
                    $tmp_row = $row->toArray();
                    foreach ($row as $key => $val) {
                        if (in_array($key, $this->getEncryptList())) {
                            $tmp_row[$key] = $this->decrypt($val);
                        }
                    }
                    array_push($this->resultdata, $tmp_row);
                }
            } else {
                $this->resultdata = array();
            }
            
        } catch(Exception $e) {
            // 例外発生
            $this->exception = $e;
            return false;
        }
        
        return true;
    }
    
    // whereの検索条件でヒットした総数を返却
    public function getAllCount() {
        return $this->allcount;
    }
    
    // fetchで取得したデータを返却
    public function getRows() {
        return $this->resultdata;
    }
    
    // 例外発生時のオブジェクトを返却
    public function getLastException() {
        return $this->exception;
    }
    
    // 内部変数をクリアする
    public function clear() {
        $this->allcount = 0;
        $this->resultdata = array();
        $this->exception = null;
        
        return;
    }
    
    // 配列からテーブルカラムの値のみ絞り込む
    public function squeeze_columns($org_data, $model) {
        $table_columns = Schema::getColumnListing($model->getTable());
        $squeeze_data = array();
        
        foreach($org_data as $key => $value) {
            if (in_array($key, $table_columns)) {
                $squeeze_data[$key] = $value;
            }
        }
        
        return $squeeze_data;
    }
    
    // 暗号化
    public function encrypt($src_string) {
        $key = 'bc0825';
        return openssl_encrypt($src_string,'aes-256-ecb',$key);
    }
    
    // 複合化
    public function decrypt($src_string) {
        $key = 'bc0825';
        return openssl_decrypt($src_string,'aes-256-ecb',$key);
    }
}
